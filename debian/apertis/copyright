Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2011, Willem-Hendrik Thiart
 2003, Danga Interactive, Inc.
License: BSD-3-clause

Files: aclocal.m4
Copyright: 1996-2021, Free Software Foundation, Inc.
License: (FSFULLR and/or GPL-2+) with Autoconf-data exception

Files: compile
 depcomp
 missing
Copyright: 1996-2021, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: configure
Copyright: 1992-1996, 1998-2017, 2020, 2021, Free Software Foundation
License: FSFUL

Files: crc32c.c
Copyright: 2013, 2015, Mark Adler
License: Zlib

Files: daemon.c
Copyright: 1990, 1993, The Regents of the University of California.
License: BSD-3-clause

Files: debian/*
Copyright: 2003-2009 Jay Bonci <jaybonci@debian.org>
            2009-2014 David Martínez Moreno <ender@debian.org>
            2019-2021 Chris Lamb <lamby@debian.org>
License: GPL-2

Files: install-sh
Copyright: 1994, X Consortium
License: X11

Files: itoa_ljust.c
Copyright: 2016, Arturo Martin-de-Nicolas
License: BSD-3-clause

Files: md5.c
 md5.h
Copyright: 1999, 2000, 2002, Aladdin Enterprises.
License: Zlib

Files: memcached_dtrace.d
 protocol_binary.h
Copyright: <2008>, Sun Microsystems, Inc.
License: BSD-3-clause

Files: murmur3_hash.c
 murmur3_hash.h
Copyright: no-info-found
License: public-domain

Files: proxy_ring_hash.c
Copyright: 2022, Cache Forge LLC
 2007, Last.fm
License: BSD-3-clause

Files: queue.h
Copyright: 1991, 1993
License: BSD-3-clause

Files: scripts/memcached-tool
Copyright: no-info-found
License: public-domain

Files: vendor/lua/doc/readme.html
Copyright: 1994-2021, Lua.org, PUC-Rio.
License: Expat

Files: vendor/mcmc/*
Copyright: Client for MemCached
 2021, Cache Forge LLC.
License: BSD-3-clause

Files: xxhash.h
Copyright: 2012-2020, Yann Collet
License: BSD-2-clause

Files: Makefile.in base64.c base64.h bipbuffer.c crawler.c crc32c.h doc/* doc/memcached.1 itoa_ljust.h jenkins_hash.c memcached.c scripts/* scripts/damemtop slab_automove.c slab_automove_extstore.c t/* vendor/* vendor/lua/doc/logo.gif vendor/lua/src/* vendor/mcmc/README.md
Copyright: Danga Interactive 2003-2014
License: BSD-3-clause
